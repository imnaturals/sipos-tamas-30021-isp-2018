package e30021.Sipos.Tamas.l4.ex6;
public class Employee {
    int age;
    String name;
    String address;
    int workHour;


    public Employee(int age,String name, String address, int workHour){
        this.age=age;
        this.name=name;
        this.address=address;
        this.workHour=workHour;
    }
    public int getAge(){
        return age;
    }
    public String getName(){
        return name;
    }
    public String getAddress(){
        return address;
    }
    public int getWorkHour(){
        return workHour;
    }
    public String toString(){
        return "Name "+getName()
                +"\n address "+getAddress()
                +"\n age "+getAge()
                +"\n workhour "+getWorkHour();
    }

    public static void main(String[] args) {
       Developer m=new Developer(30,"Bob","LA",3,"asd","C",5);
        System.out.println(m.toString());
        Developer n=new Developer(20,"Jhonny","NY",8,"lnko","Java",9);
        System.out.println("new Employee");
        System.out.println(n.toString());
        Manager me=new Manager(40,"Andrew","Ma",9,3,"3D Modelling",534);
        System.out.println(me.toString());
    }

}
