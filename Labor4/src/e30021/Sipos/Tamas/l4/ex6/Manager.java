package e30021.Sipos.Tamas.l4.ex6;
public class Manager extends Employee{
    int lookingFor;
    String skillRequired;
    int timeLeft;

    Manager(int age,String name, String address, int workHour,int lookingFor,String skillRequired,int timeLeft){
        super(age,name,address,workHour);
        this.lookingFor=lookingFor;
        this.skillRequired=skillRequired;
        this.timeLeft=timeLeft;

    }
    public int getLookingFor(){
        return lookingFor;
    }
    public String getSkillRequired(){
        return skillRequired;
    }
    public int getTimeLeft(){
        return timeLeft;
    }
    public String toString(){
        return "Employee "+super.toString()+" how many people are we looking for: "+getLookingFor()
                +"\n What skills do we need : "+getSkillRequired()
                +"\n time left(in hour) : "+getTimeLeft();
    }
    }

