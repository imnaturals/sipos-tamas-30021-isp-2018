package e30021.Sipos.Tamas.l4.ex6;
public class Developer extends Employee{
       String projectName;
       String codingLang;
       int howMany;



      public Developer(int age,String name, String address, int workHour,String projectName,String codingLang,int howMany){
          super(age,name,address,workHour);
           this.projectName=projectName;
           this.codingLang=codingLang;
           this.howMany=howMany;
       }
       public String getProjectName(){
           return projectName;
       }
       public String getCodingLang(){
           return codingLang;
       }

       public int getHowMany(){
           return howMany;
       }
       public String toString(){
           return "Employee "+super.toString()+" project name is: "+getProjectName()
                   +"\n What language is being worked on: "+getCodingLang()
                   +"\n how many people in the group: "+getHowMany();
       }
}
