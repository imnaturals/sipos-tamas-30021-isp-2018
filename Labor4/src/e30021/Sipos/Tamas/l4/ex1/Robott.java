package e30021.Sipos.Tamas.l4.ex1;
public class Robott {
    Engine robotEngine;

    public Robott() {
        robotEngine = new Engine();
    }

    public void moveRobot(int x, int y) {
        robotEngine.step(x, y);
    }
}
