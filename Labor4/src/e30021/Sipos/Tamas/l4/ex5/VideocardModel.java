package e30021.Sipos.Tamas.l4.ex5;
public class VideocardModel extends Videocard {
    String type;
    public VideocardModel(String name){
        super(name);
        type="Nvidia";
    }
    public VideocardModel(String name,String type){
        super(name);
        this.type=type;
    }
    public String getType(){
        return type;
    }
    public String toString(){
        return "the videocard "+super.toString()+" and its type is : "+getType();
    }
}
