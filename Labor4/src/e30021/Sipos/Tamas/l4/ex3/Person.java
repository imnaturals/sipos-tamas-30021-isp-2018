package e30021.Sipos.Tamas.l4.ex3;
public class Person {
    String fullName;
    int age;
    public Person(int age, String fullName){
        this.age=age;
        this.fullName=fullName;
    }

    public Person() {
    fullName="KEv";
    age=19;
    }

    int getAge(){
        return this.age;
    }
    String getFullName(){
        return this.fullName;
    }
    public String toString(){
        return "The person's name is:"+this.fullName+" and its age is:"+getAge();
    }

    public static void main(String[] args) {
        Person Kevin=new Person();
        System.out.println(Kevin.toString());
        Person n = new Professor(4,"Jhon",32);
        System.out.println(n.toString());
        Person m=new Student(9,"Alpha",20);
        System.out.println(m.toString());

    }
}
