package e30021.Sipos.Tamas.l4.ex3;
public class Professor extends Person {
    int professorNumber;


    public Professor(int number,String fullName,int age){
        super(age,fullName);
        professorNumber = number;
    }



    public int getProfessorNumber() {
        return professorNumber;
    }

    public String toString() {
        return "The person is a professor and his name is :" + getFullName() + " and its age is:" + getAge() + "and its number is" + getProfessorNumber();
    }
}
