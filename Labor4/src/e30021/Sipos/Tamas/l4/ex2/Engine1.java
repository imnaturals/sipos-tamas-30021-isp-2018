package e30021.Sipos.Tamas.l4.ex2;
class Engine1{
    void step (int x,int y ) {
        start();
        checkDirection(x);
        execute(x);
        checkDdirection(y);
        execute(y);
        stop();
    }

    private void start(){
        System.out.println("Start engine.");
    }
    private void stop(){
        System.out.println("Stop engine.");
    }

    private void checkDirection(int x){
        if(x<0)
            System.out.println("Moving to the left.");
        else if(x>0)
            System.out.println("Moving to the right.");
    }


    private void checkDdirection(int y){
        if(y<0)
            System.out.println("Moving down");
        else if(y>0)
            System.out.println("Moving up");
    }



    private void execute(int s){
        System.out.println("Moving "+Math.abs(s)+" steps.");
    }

}
