
package e30021.Sipos.Tamas.l4.ex2;


class BattleField {
    Robot r1;
    Robot r2;

    BattleField(){
        r1 = new Robot();
        r2 = new Robot();
    }
    public void play(){
        r1.moveRobot(35,20,9);

        r2.moveRobot(-10,-8,6);
    }
    public static void main(String[] args){
        BattleField game = new BattleField();
        game.play();
    }
}
