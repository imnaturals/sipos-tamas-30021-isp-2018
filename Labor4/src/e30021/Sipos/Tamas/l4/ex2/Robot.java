package e30021.Sipos.Tamas.l4.ex2;
public class Robot {
    Engine1 robotEngine;
    public Robot(){
        robotEngine = new Engine1();
    }

    public void moveRobot(int x,int y ,int obj){
        carryRobot(obj);
        robotEngine.step(x,y );
        putDownRobot(obj);
    }

    public static void carryRobot(int x) {
        if (x!=0) {
            Transport.lift(x);

        }



    }
    public static void putDownRobot(int x) {
        if (x!=0) {
            Transport.putDown();

        }

    }

}
