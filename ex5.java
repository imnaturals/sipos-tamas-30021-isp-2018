import java.util.Random;
public class ex5 {
    public static void main(String[] args) {

        int[] n=new int[10];
        Random t = new Random();

        for (int i = 0; i < 10; i++) {
            n[i] = 1 + t.nextInt(100);
        }
        System.out.println("Before sorting:");
        for (int i = 0; i < 10; i++) {
            System.out.println("n[" + i + "] = " + n[i]);
        }

        boolean sorted = false;
        while (!sorted) {
            sorted = true;
            for (int i=0; i < 9; i++) {
                if (n[i] > n[i+1]) {
                    int temp = n[i];
                    n[i] = n[i+1];
                    n[i+1] = temp;
                    sorted = false;
                }
            }
        }
        System.out.println("After sorting:");
        for (int i = 0; i < 10; i++) {
            System.out.println("n[" + i + "] = " + n[i]);

        }
    }
}
