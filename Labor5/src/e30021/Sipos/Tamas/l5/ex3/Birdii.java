package e30021.Sipos.Tamas.l5.ex3;

class Birdii implements Animal {
    public void move(){
        System.out.println("The bird is moving.");
    }

    @Override
    public void relocateBirds() {

    }

    @Override
    public Birdii createBird() {
        return null;
    }
}

class Penguin extends Birdii implements Animal {
    public void move(){
        System.out.println("The PENGUIN is swiming.");
    }
}

class Goose extends Birdii implements Animal {
    public void move(){
        System.out.println("The GOOSE is flying.");
    }
}
