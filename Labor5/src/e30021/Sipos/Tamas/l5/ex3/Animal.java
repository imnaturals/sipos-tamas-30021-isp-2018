package e30021.Sipos.Tamas.l5.ex3;



public interface Animal {
    void move();
    void relocateBirds();
    Birdii createBird();
}
