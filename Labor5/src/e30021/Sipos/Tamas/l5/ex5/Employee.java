package e30021.Sipos.Tamas.l5.ex5;

public class Employee extends Person {
    String address;
    int workHour;


    public Employee(int age, String fullName, String address, int workHour) {
        super(age, fullName);
        this.address = address;
        this.workHour = workHour;
    }

    public Employee() {
        address="Lulvalley";
        workHour=7;

    }


    public String getAddress() {
        return address;
    }

    public int getWorkHour() {
        return workHour;
    }

    public String toString() {
        return " " + super.toString()
                + "\n address " + getAddress()
                + "\n age " + getAge()
                + "\n workhour " + getWorkHour();
    }
}
