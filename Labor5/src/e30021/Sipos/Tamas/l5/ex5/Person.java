package e30021.Sipos.Tamas.l5.ex5;

public class Person {
    String fullName;
    int age;
    public Person(int age, String fullName){
        this.age=age;
        this.fullName=fullName;
    }

    public Person() {
        fullName="KEv";
        age=19;
    }

    int getAge(){
        return this.age;
    }
    String getFullName(){
        return this.fullName;
    }
    public String toString(){
        return "The person's name is:"+getFullName()+" and its age is:"+getAge();
    }

    public static void main(String[] args) {
        Person Kevin=new Person();
        System.out.println(Kevin.toString());
        Person kevin=new Employee();
        System.out.println(kevin.toString());
        Person kevin1=new Faculty();
        System.out.println(kevin1.toString());



    }
}
