package e30021.Sipos.Tamas.l5.ex5;

public class Faculty extends Employee {
    public static int Professor=1;
    public static int Asistant=2;
    private int rank;
    public Faculty(int rank){
        this.rank=rank;

    }
    public Faculty (){
        rank=1;
    }
    public String getRank(){
        return (rank==1) ? "Professor"
                : "Assistant";
    }

    public String toString(){
        return "" +super.toString()+" the rank is "+getRank();
    }


}
