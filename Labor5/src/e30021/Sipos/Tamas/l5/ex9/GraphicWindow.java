package e30021.Sipos.Tamas.l5.ex9;

import javax.swing.*;
import java.awt.*;


    public class GraphicWindow extends JFrame {

        Shape[] shapes = new Shape[10];


        public GraphicWindow() {
            this.setTitle("Drawing board");
            this.setSize(300, 500);
            this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            this.setVisible(true);
        }

        public void addShape(Shape s1) {
            for (int i = 0; i < shapes.length; i++) {
                if (shapes[i] == null) {
                    shapes[i] = s1;
                    break;
                }
            }
            this.repaint();
        }
        public void paint(Graphics g) {
            for (int i = 0; i < shapes.length; i++) {
                if (shapes[i] != null)
                    shapes[i].draw(g);
            }
        }


        public static void main(String[] args) {
            GraphicWindow gg=new GraphicWindow();
            Shape s3 = new Circle(Color.green,70);
            gg.addShape(s3);
           Shape s2=new Point(Color.red,0,10);
            gg.addShape(s2);

        }
    }
