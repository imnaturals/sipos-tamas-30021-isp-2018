package e30021.Sipos.Tamas.l5.ex9;

import java.awt.*;
public class Circle extends Shape{
    private int r;
    public Circle(Color color,int r){
        super(color);
        this.r=r;

    }
    public int getR(){
        return r;
    }
    public void draw(Graphics g){
        System.out.println("Drawing a circle at radius "+getR());
        g.drawOval(40,40,r,r);



    }
}
