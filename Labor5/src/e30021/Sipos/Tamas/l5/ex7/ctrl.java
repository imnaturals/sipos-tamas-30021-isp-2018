package e30021.Sipos.Tamas.l5.ex7;
public class ctrl {
    private String location;
    private Sensorr tempSensor;

    public ctrl(String location, Sensorr tempSensor) {
        this.location = location;
        this.tempSensor = tempSensor;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Sensorr getTempSensor() {
        return tempSensor;
    }

    public void setTempSensor(Sensorr tempSensor) {
        this.tempSensor = tempSensor;
    }
    public void checkTemperature(){
        System.out.println("the temp is "+tempSensor);
    }

    @Override
    public String toString() {
        return " " +
                "location=" + location +
                ", tempSensor=" + tempSensor;
    }

    public static void main(String[] args) {
        Sensorr s=new Sensorr(3,"asd");
        System.out.println(s.toString());
        ctrl r=new ctrl("free",s);
        System.out.println(r.toString());
        r.checkTemperature();

    }
}


