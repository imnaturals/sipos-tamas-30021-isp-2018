package e30021.Sipos.Tamas.l5.ex10;

import java.awt.*;
public class R extends S {

    private int length;
    private int width;
    private int x;
    private int y;

    public R(Color color, int length,int width,int x,int y) {
        super(color);
        this.length = length;
        this.width=width;
        this.x=x;
        this.y=y;
    }
    public int getLength(){
        return this.length;
    }
    public int getWidth(){
        return width;
    }
    public void draw(Graphics g){
        System.out.println("Drawing a rectangel "+getLength()+" "+getColor().toString());
        g.drawRect(x,y,width,length);
    }
}

