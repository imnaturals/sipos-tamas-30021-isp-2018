package e30021.Sipos.Tamas.l5.ex10;

import javax.swing.*;
import java.awt.*;
public class G extends JFrame{
    S[] s = new S[10];


    public G() {
        this.setTitle("Drawing board");
        this.setSize(300, 500);
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void addShape(S s1) {
        for (int i = 0; i < s.length; i++) {
            if (s[i] == null) {
                s[i] = s1;
                break;
            }
        }
        this.repaint();
    }
    public void paint(Graphics g) {
        for (int i = 0; i < s.length; i++) {
            if (s[i] != null)
                s[i].draw(g);
        }
    }


    public static void main(String[] args) {
        G gg=new G();
        S s3 = new C(Color.black,70,90,90);
        gg.addShape(s3);
        R s5= new R(Color.black,30,90,80,80);
        gg.addShape(s5);

    }
}
