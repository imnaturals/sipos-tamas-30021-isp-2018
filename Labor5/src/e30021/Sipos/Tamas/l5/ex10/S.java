package e30021.Sipos.Tamas.l5.ex10;

import java.awt.*;
public abstract class S {
    private Color color;

    public S(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}

