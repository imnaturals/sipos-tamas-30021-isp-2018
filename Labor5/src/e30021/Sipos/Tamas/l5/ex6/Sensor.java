package e30021.Sipos.Tamas.l5.ex6;

public class Sensor {
    private int value;
    private String location="Elf";

    public int getValue(){
        return value;
    }
    public void  setValue(int v){
        this.value=v;
    }
    public void displaySensorInfo(){
        System.out.println(" the value is "+value+" and the location is "+location);
    }

    public static void main(String[] args) {
        Sensor s=new Sensor();
       s.setValue(5);
        s.getValue();
        s.displaySensorInfo();
       // System.out.println("the value of the sensor is "+s.getValue());
    }
}
