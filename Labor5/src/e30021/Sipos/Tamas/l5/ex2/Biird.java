package e30021.Sipos.Tamas.l5.ex2;

abstract class Biird {
    public abstract void move();
}

    class Penguin extends Biird {
        public void move(){
            System.out.println("The PENGUIN is swiming.");
        }
    }

    class Goose extends Biird {
        public void move(){
            System.out.println("The GOOSE is flying.");
        }
    }
