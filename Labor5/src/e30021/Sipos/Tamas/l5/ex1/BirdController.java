package e30021.Sipos.Tamas.l5.ex1;

public class BirdController{
    Bird[] birds = new Bird[3];
    BirdController(){
        birds[0] = createBird();
        birds[1] = createBird();
        birds[2] = createBird();
    }
    public void relocateBirds(){
        for(int i=0;i<birds.length;i++)
            birds[i].move();
    }

    private Bird createBird(){
        int i = (int)(Math.random()*10);
        if(i<5)
            return new Penguin();
        else if(i>8)
            return new Goose();
        else
        return new Turtle();
    }

    public static void main(String [] args){
        BirdController bc = new BirdController();
        bc.relocateBirds();
    }
}
