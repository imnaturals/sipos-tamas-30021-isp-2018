package e30021.Sipos.Tamas.l3.ex5;
public class Rectangle {
    public int length;
    public int width;
    Rectangle(){
        length=10;
        width=5;
    }
    Rectangle(int len,int w){
        length=len;
        width=w;
    }
    public double getLength(){
        return length;
    }
    public double getWidth(){
        return width;
    }
    public double getArea(){
        return length*width;
    }
}
