package e30021.Sipos.Tamas.l3.ex2;
public class Engine {
    String fuellType;
    long capacity;
    boolean active;
    int speed;

    Engine(int capacity,boolean active,int speed){
        this.capacity = capacity;
        this.active = active;
        this.speed=speed;
    }
    Engine(int capacity,boolean active,int speed, String fuellType){
        this(capacity,active,speed);
        this.fuellType = fuellType;
    }
    Engine(){
        this(2000,false,40,"Diesel");
    }
    void print(){
        System.out.println("Engine: capacity="+this.capacity+" fuell="+fuellType+" active="+active+ " speed="+speed);
    }
    public static void main(String[] args) {
        Engine tdi = new Engine();
        Engine i16 = new Engine(1600,false,30,"petrol");
        Engine d30 = new Engine(3000,true,20,"diesel");
        tdi.print();i16.print();d30.print();
    }
}
