package e30021.Sipos.Tamas.l3.ex1;

//ex1
public class Vehicle {
    String model;
    int speed;
    String fuelType;

    Vehicle(){
        model="BMW";
        speed=6;
        fuelType="Diesel";
    }
    Vehicle(String m,int s,String f)
    {
        model=m;
        speed=s;
        fuelType=f;
    }
    public String getModel(){
        return model;

    }
    public String getFuelType(){
        return fuelType;
    }
    public int getSpeed(){
        return speed;
    }

    public void accelerate (){
        speed+=10;
    }
    public void brake(){
        speed-=5;
    }
    public void stop(){
        speed=0;
    }
    public String toString(){
        return "the car's model is:"+model+" and its speed = "+speed+"and its fuel is typeof"+fuelType;
    }

    public static void main(String[] args) {
        Vehicle v1=new Vehicle();
        System.out.println("The modeil is: "+v1.getModel()+" the speed is:"+v1.getSpeed()+" and the fueltype is: "+v1.getFuelType());
        Vehicle v2=new Vehicle("audi",10,"benzin");
        System.out.println(v2);
        for(int i=0;i<5;i++){
            v2.accelerate();
            if (i==2)
                v2.brake();
        }
        System.out.println(v2);
        Vehicle v3=new Vehicle("Golf",40,"Diesel");
        System.out.println(v3);
        do{
            v3.brake();
        }while(v3.speed!=0);
        System.out.println(v3);
    }

}